"""WebCoursework1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from ProfessorRating.views import register, list, view, average, rate, myLogin, myLogout

urlpatterns = [
    path('admin/', admin.site.urls),

	path('api/register/', register),
	#List of All modules
	path('api/list/', list),
	#Average ratings of all professors
	path('api/view/', view),
	#Average rating of a given professor for a given module FIX HOW PARAMS PASSED
	path('api/average/<str:professor_id>/<str:module_code>/', average),
	#Give a rating on a given professor for a given module instance
	path('api/rate/', rate),
	#login
	path('api/login/', myLogin),
	#logout
	path('api/logout/', myLogout)

]