certifi==2021.10.8
Django==2.2.5
django-extensions==3.1.5
graphviz==0.19.1
numpy==1.21.5
PyGraph==0.2.1
pygraphviz==1.7
pytz==2021.3
sqlparse @ file:///tmp/build/80754af9/sqlparse_1602184451250/work
viz==0.0.5
