# Generated by Django 2.2.5 on 2022-03-15 12:20

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ProfessorRating', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModuleInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.CharField(max_length=4)),
                ('semester', models.CharField(max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(5)])),
                ('module_instance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ProfessorRating.ModuleInstance')),
            ],
        ),
        migrations.DeleteModel(
            name='Student',
        ),
        migrations.RemoveField(
            model_name='module',
            name='professors',
        ),
        migrations.RemoveField(
            model_name='module',
            name='semester',
        ),
        migrations.RemoveField(
            model_name='module',
            name='year',
        ),
        migrations.RemoveField(
            model_name='professor',
            name='identifier',
        ),
        migrations.AddField(
            model_name='professor',
            name='professor_id',
            field=models.CharField(default=1, max_length=3),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rating',
            name='professor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ProfessorRating.Professor'),
        ),
        migrations.AddField(
            model_name='rating',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='moduleinstance',
            name='module',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ProfessorRating.Module'),
        ),
        migrations.AddField(
            model_name='moduleinstance',
            name='professors',
            field=models.ManyToManyField(to='ProfessorRating.Professor'),
        ),
    ]
