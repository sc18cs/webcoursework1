from django.http import HttpResponse
from ProfessorRating.models import ModuleInstance, Module, Professor, Rating
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
import json
from django.db.models import Avg
from django.db import IntegrityError



def register(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		print(username)
		email = request.POST.get('email')
		print(email)
		password = request.POST.get('password')
		print(password)
		password2 = request.POST.get('password2')
		print(password2)
		if None not in (username, email, password, password2):
			if password == password2:
				try:
					newUser = User(username=username, email=email, password=password)
					newUser.save()
					login(request, newUser)
					returnResponse = {"message": "Registration successful. You are now logged in"}
				except IntegrityError:
					returnResponse = {"message": "User already exists"}
			else:
				returnResponse = {"message": "Passwords did not match"}
		else:
			returnResponse = {"message": "Fill all registration fields"}

	return HttpResponse(json.dumps(returnResponse))


def list(request):
	if not request.user.is_authenticated:
		returnResponse = {"objects": "Not Logged In"}
		return HttpResponse(json.dumps(returnResponse))
	if request.method == 'GET':
		module_instances = ModuleInstance.objects.all()
		#create own response array
		return_Array = []
		for item in module_instances:
			professor_Array = []
			for professor in item.professors.all():
				newProfessor = {"professor_id": professor.professor_id,
								 "professor_name": professor.name}
				professor_Array.append(newProfessor)
				print(professor_Array)

			newInstance = {"module_code": item.module.code,
			"module_name": item.module.name,
			"year": item.year,
			"semester": item.semester,
			"professors": professor_Array}
			return_Array.append(newInstance)

		returnResponse = {"objects": return_Array}

	return HttpResponse(json.dumps(returnResponse))


def view(request):
	if not request.user.is_authenticated:
		returnResponse = {"objects": "Not Logged In"}
		return HttpResponse(json.dumps(returnResponse))
	if request.method == 'GET':
		professors = Professor.objects.all()
		returnArray = []
		for professor in professors:
			ratings = Rating.objects.filter(professor=professor)
			averageRating = ratings.aggregate(Avg('score'))
			averageRatingFloat = averageRating["score__avg"]
			if averageRatingFloat%int(averageRatingFloat) >= 0.5:
				averageRatingFloat = int(averageRatingFloat)+1

			newRating = {"professor_name": professor.name,
						   "professor_id": professor.professor_id,
						   "Rating": averageRatingFloat}

			returnArray.append(newRating)

	returnResponse = {"objects": returnArray}

	return HttpResponse(json.dumps(returnResponse))


def average(request, professor_id, module_code):
	errorCounter = 0
	if not request.user.is_authenticated:
		returnResponse = {"objects": "Not Logged In"}
		return HttpResponse(json.dumps(returnResponse))
	if request.method == 'GET':
		try:
			professor = Professor.objects.get(professor_id=professor_id)
			module = Module.objects.get(code=module_code)
			moduleInstances = ModuleInstance.objects.filter(module=module, professors=professor)
			if not moduleInstances:
				errorCounter = errorCounter + 1
				returnResponse = {"objects": "Module Instance does not exist",
								  "error counter": errorCounter}
			else:
				ratings = Rating.objects.filter(professor=professor, module_instance__in=moduleInstances)
				if not ratings:
					errorCounter = errorCounter + 1
					returnResponse = {"objects": "Ratings don't exist",
									  "error counter": errorCounter}
		except Professor.DoesNotExist:
			errorCounter = errorCounter + 1
			returnResponse = {"objects": "Professor does not exist",
							  "error counter": errorCounter}
		except Module.DoesNotExist:
			errorCounter = errorCounter + 1
			returnResponse = {"objects": "Module does not exist",
							  "error counter": errorCounter}
		print(errorCounter)
		if errorCounter == 0:
			averageRating = ratings.aggregate(Avg('score'))
			averageRatingFloat = averageRating["score__avg"]
			if averageRatingFloat%int(averageRatingFloat) >= 0.5:
				averageRatingFloat = int(averageRatingFloat)+1

			newRating = {"professor_name": professor.name,
						 "professor_id": professor.professor_id,
						 "module_code": module.code,
						 "Rating": averageRatingFloat}
			returnArray = []
			returnArray.append(newRating)

			returnResponse = {"objects": returnArray,
							  "error counter": errorCounter}
			return HttpResponse(json.dumps(returnResponse))
		#return error counter

	return HttpResponse(json.dumps(returnResponse))



def rate(request):
	errorCounter = 0
	if not request.user.is_authenticated:
		returnResponse = {"objects": "Not Logged In"}
		return HttpResponse(json.dumps(returnResponse))
	if request.method == 'POST':
		professor_id = request.POST.get('professor_id')
		module_code = request.POST.get('module_code')
		year = request.POST.get('year')
		semester = request.POST.get('semester')
		score = request.POST.get('score')
		try:
			professor = Professor.objects.get(professor_id=professor_id)
			module = Module.objects.get(code=module_code)
			moduleInstance = ModuleInstance.objects.get(module=module, year=year, semester=semester)
			if professor in moduleInstance.professors.all():
				newRating = Rating.objects.create(professor=professor, module_instance=moduleInstance, student=request.user,
												  score=score)
				newRating.save()
				returnResponse = {"message": "Rating Successful"}
			else:
				returnResponse = {"message": "Professor does not teach on this module instance"}
		except Professor.DoesNotExist:
			returnResponse = {"message": "Professor does not exist"}
		except Module.DoesNotExist:
			returnResponse = {"message": "Module does not exist"}
		except ModuleInstance.DoesNotExist:
			returnResponse = {"message": "Module Instance does not exist"}

	print(returnResponse)
	print(returnResponse["message"])
	return HttpResponse(json.dumps(returnResponse))



def myLogin(request):
	if request.method == "POST":
		username = request.POST.get('Username')
		print(username)
		password = request.POST.get('Password')
		print(password)
		user = authenticate(request, username=username, password=password)
		print(user)
		if user is not None:
			login(request, user)
			returnResponse = {"message": "You are now logged in"}
		else:
			returnResponse = {"message": "Invalid Credentials"}

	return HttpResponse(json.dumps(returnResponse))



def myLogout(request):
	if not request.user.is_authenticated:
		returnResponse = {"objects": "Not Logged In"}
		return HttpResponse(json.dumps(returnResponse))
	logout(request)
	returnResponse = {"message": "You are now logged out"}
	return HttpResponse(json.dumps(returnResponse))




