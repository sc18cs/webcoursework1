from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class Professor(models.Model):
	name = models.CharField(max_length=30)
	professor_id = models.CharField(max_length=3, unique=True)

	def __str__(self):
		return u'%s %s' % (self.professor_id, self.name)

class Module(models.Model):
	name = models.CharField(max_length=30)
	code = models.CharField(max_length=3, unique=True)

	def __str__(self):
		return u'%s %s' % (self.code, self.name)

class ModuleInstance(models.Model):
	module = models.ForeignKey(Module, on_delete=models.CASCADE)
	year = models.CharField(max_length=4)
	semester = models.CharField(max_length=1)
	professors = models.ManyToManyField(Professor)

	class Meta:
		constraints = [
			models.UniqueConstraint(fields= ['module', 'year', 'semester'], name='Unique_Module_Instance')
		]

	def __str__(self):
		professors_string = ", ".join(str(segment) for segment in self.professors.all())
		return u'%s %s %s %s' % (self.module, self.year, self.semester, professors_string)


class Rating(models.Model):
	professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
	student = models.ForeignKey(User, on_delete=models.CASCADE)
	module_instance = models.ForeignKey(ModuleInstance, on_delete=models.CASCADE)
	score = models.IntegerField(validators=[MinValueValidator(1),MaxValueValidator(5)])


	def __str__(self):
		return u'%s %s' % (self.professor, self.student)
