from django.apps import AppConfig


class ProfessorratingConfig(AppConfig):
    name = 'ProfessorRating'
